document.getElementById("hello_text").textContent = "はじめてのJavaScript";

/* コメントアウトしたコード -> 1秒ごとにブロックが落ちていくアニメーションまでの実装（関数を使ってまとめる前）
var count = 0; // 変数countを定義
setInterval(function() { // countを1ずつ増加
count++;
document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")"; // 何回目かをセルに文字にまとめて表示する
var td_array = document.getElementsByTagName("td"); // tdタグを取得
var cells = []; // cell格納配列
var index = 0;
for (var row = 0; row < 20; row++) {
cells[row] = []; // 配列の要素を2次元配列にする
for (var col = 0; col < 10; col++) {
cells[row][col] = td_array[index];
index++;
}
}

for (var i = 0; i < 10; i++) {
cells[19][i].className = ""; // 最下行クラスを空にする 
}

for (var row = 18; row >= 0; row--) {
for (var col = 0; col < 10; col++) {
if (cells[row][col].className !== "") {
  cells[row + 1][col].className = cells[row][col].className; // 下から二番目の行から繰り返しクラスを下げていく
  cells[row][col].className = "";
}
}
}
}, 1000);　
*/

var count = 0; // count変数
var cells; // ゲーム盤を示す変数

var blocks = { // ブロックのパターンを追加
    i: {
        class: "i",
        pattern: [
            [1, 1, 1, 1]
        ]
    },
    o: {
        class: "o",
        pattern: [
            [1, 1],
            [1, 1]
        ]
    },
    t: {
        class: "t",
        pattern: [
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s: {
        class: "s",
        pattern: [
            [0, 1, 1],
            [1, 1, 0]
        ]
    },
    z: {
        class: "z",
        pattern: [
            [1, 1, 0],
            [0, 1, 1]
        ]
    },
    j: {
        class: "j",
        pattern: [
            [1, 0, 0],
            [1, 1, 1]
        ]
    },
    l: {
        class: "l",
        pattern: [
            [0, 0, 1],
            [1, 1, 1]
        ]
    }
};

loadTable();
setInterval(function() {
    count++;
    document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")"; // はじめてのJavaScript(?)の?数字を変化
    if (hasFallingBlock()) { // 落下中のブロックの有無を確認
        fallBlocks(); // ある場合、ブロックを落とす
    } else { // ない場合
        deleteRow(); // 揃っている行を消す
        generateBlock(); // ランダムにブロックを生成
    }
}, 1000);

/*　関数に分ける前のコード

function loadTable() { // ゲーム盤を変数cellsにまとめる
cells = [];
var td_array = document.getElementsByTagName("td");
var index = 0;
for (var row = 0; row < 20; row++) {
cells[row] = []; // 配列の要素を2次元配列にする
for (var col = 0; col < 10; col++) {
  cells[row][col] = td_array[index];
  index++;
  }
}

}

function fallBlocks() { // ブロックを落とす
for (var i = 0; i < 10; i++) {
cells[19][i].className = ""; // 最下行クラスを空にする 
}

for (var row = 18; row >= 0; row--) {
for (var col = 0; col < 10; col++) {
  if (cells[row][col].className !== "") {
    cells[row + 1][col].className = cells[row][col].className; // 下から二番目の行から繰り返しクラスを下げていく
    cells[row][col].className = "";
  }
}
}
}　*/




function loadTable() { // ゲーム盤を変数cellsにまとめる

    cells = [];
    var td_array = document.getElementsByTagName("td");
    var index = 0;
    for (var row = 0; row < 20; row++) {
        cells[row] = []; // 配列の要素を2次元配列にする
        for (var col = 0; col < 10; col++) {
            cells[row][col] = td_array[index];
            index++;
        }
    }

}


function fallBlocks() {
    for (var col = 0; col < 10; col++) {
        if (cells[19][col].blockNum === fallingBlockNum) {　 // 底面にブロックがついているかどうか
            isFalling = false;
            return;
        }
    }
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) { // 1cell下に別なブロックがあるかどうかを判断
                    isFalling = false;
                    return;
                }
            }
        }
    }
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) { // 繰り返してクラスを下げる
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

var isFalling = false;　 // falseで初期化

function hasFallingBlock() {
    // 落下中のブロックがあるかを確認
    return isFalling;
}

function deleteRow() {
    // そろっている行を消す
}

var fallingBlockNum = 0;　 // 変数fallingBlockNumでブロック生成時の番号を保持
function generateBlock() {
    // ランダムにブロックを生成する
    // ランダムにパターンを1つ選ぶ
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;
    // 選んだパターンをもとにブロックを配置
    var pattern = nextBlock.pattern;
    for (var row = 0; row < pattern.length; row++) {
        for (var col = 0; col < pattern[row].length; col++) {
            if (pattern[row][col]) {
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }

    isFalling = true; // 落下しているブロック
    fallingBlockNum = nextFallingBlockNum;
}


function moveRight() {
    // ブロックを右へ
}


function moveLeft() {
    // ブロックを左へ
}