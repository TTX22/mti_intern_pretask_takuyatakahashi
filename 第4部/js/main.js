document.getElementById("hello_text").textContent = "はじめてのJavaScript";
document.addEventListener("keydown", onKeyDown); // キーボード監視用

function onKeyDown(event) { // Key入力->呼び出し用function
    if (event.keyCode === 37) {
        moveLeft();
    } else if (event.keyCode === 39) {
        moveRight();
    }
}

/* コメントアウトしたコード -> 1秒ごとにブロックが落ちていくアニメーションまでの実装（関数を使ってまとめる前）
var count = 0; // 変数countを定義
setInterval(function() { // countを1ずつ増加
count++;
document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")"; // 何回目かをセルに文字にまとめて表示する
var td_array = document.getElementsByTagName("td"); // tdタグを取得
var cells = []; // cell格納配列
var index = 0;
for (var row = 0; row < 20; row++) {
cells[row] = []; // 配列の要素を2次元配列にする
for (var col = 0; col < 10; col++) {
cells[row][col] = td_array[index];
index++;
}
}

for (var i = 0; i < 10; i++) {
cells[19][i].className = ""; // 最下行クラスを空にする 
}

for (var row = 18; row >= 0; row--) {
for (var col = 0; col < 10; col++) {
if (cells[row][col].className !== "") {
cells[row + 1][col].className = cells[row][col].className; // 下から二番目の行から繰り返しクラスを下げていく
cells[row][col].className = "";
}
}
}
}, 1000);　
*/

var count = 0; // count変数
var cells; // ゲームエリアを示す

var blocks = { // ブロックのパターンを追加
    i: {
        class: "i",
        pattern: [
            [1, 1, 1, 1]
        ]
    },
    o: {
        class: "o",
        pattern: [
            [1, 1],
            [1, 1]
        ]
    },
    t: {
        class: "t",
        pattern: [
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s: {
        class: "s",
        pattern: [
            [0, 1, 1],
            [1, 1, 0]
        ]
    },
    z: {
        class: "z",
        pattern: [
            [1, 1, 0],
            [0, 1, 1]
        ]
    },
    j: {
        class: "j",
        pattern: [
            [1, 0, 0],
            [1, 1, 1]
        ]
    },
    l: {
        class: "l",
        pattern: [
            [0, 0, 1],
            [1, 1, 1]
        ]
    }
};

loadTable();
setInterval(function() { // 積みあがったらGame Over
    count++;
    document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";
    for (var row = 0; row & lt; 2; row++) {
        for (var col = 0; col & lt; 10; col++) {
            if (cells[row][col].className !== "") {
                alert("game over"); // game overアラートを送出
            }
        }
    }
    if (hasFallingBlock()) { // 落下中のブロックがあるか確認
        fallBlocks(); // ある場合->ブロックを落とす
    } else { // なければ
        deleteRow(); // そろっている行を消す
        generateBlock(); // ランダムにブロックを生成
    }
}, 1000);

/*　関数に分ける前のコード

function loadTable() { // ゲーム盤を変数cellsにまとめる
cells = [];
var td_array = document.getElementsByTagName("td");
var index = 0;
for (var row = 0; row < 20; row++) {
cells[row] = []; // 配列の要素を2次元配列にする
for (var col = 0; col < 10; col++) {
cells[row][col] = td_array[index];
index++;
}
}

}

function fallBlocks() { // ブロックを落とす
for (var i = 0; i < 10; i++) {
cells[19][i].className = ""; // 最下行クラスを空にする 
}

for (var row = 18; row >= 0; row--) {
for (var col = 0; col < 10; col++) {
if (cells[row][col].className !== "") {
cells[row + 1][col].className = cells[row][col].className; // 下から二番目の行から繰り返しクラスを下げていく
cells[row][col].className = "";
}
}
}
}　*/




function loadTable() { // ゲーム盤を変数cellsにまとめる

    cells = [];
    var td_array = document.getElementsByTagName("td");
    var index = 0;
    for (var row = 0; row < 20; row++) {
        cells[row] = []; // 配列の要素を2次元配列にする
        for (var col = 0; col < 10; col++) {
            cells[row][col] = td_array[index];
            index++;
        }
    }

}


function fallBlocks() {
    for (var col = 0; col < 10; col++) {
        if (cells[19][col].blockNum === fallingBlockNum) {　 // 底面にブロックがついているかどうか
            isFalling = false;
            return;
        }
    }
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) { // 1cell下に別なブロックがあるかどうかを判断
                    isFalling = false;
                    return;
                }
            }
        }
    }
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) { // 繰り返してクラスを下げる
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

var isFalling = false;　 // falseで初期化

function hasFallingBlock() {
    // 落下中のブロックがあるかを確認
    return isFalling;
}

function deleteRow() { // 揃った行をDelete
    for (var row = 19; row & gt; = 0; row--) {
        var canDelete = true;
        for (var col = 0; col & lt; 10; col++) {
            if (cells[row][col].className === "") {
                canDelete = false;
            }
        }
        if (canDelete) {
            // 1行Delete
            for (var col = 0; col & lt; 10; col++) {
                cells[row][col].className = "";
            }
            // 上の行のブロックをすべて1cell落とす
            for (var downRow = row - 1; row & gt; = 0; row--) {
                for (var col = 0; col & lt; 10; col++) {
                    cells[downRow + 1][col].className = cells[downRow][col].className;
                    cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
                    cells[downRow][col].className = "";
                    cells[downRow][col].blockNum = null;
                }
            }
        }
    }
}

var fallingBlockNum = 0;　 // 変数fallingBlockNumでブロック生成時の番号を保持
function generateBlock() { // ランダムにブロックを生成する
    // ランダムにパターンを1つ選ぶ
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;

    var pattern = nextBlock.pattern;　 //ブロックを配置する
    for (var row = 0; row < pattern.length; row++) {
        for (var col = 0; col < pattern[row].length; col++) {
            if (pattern[row][col]) {
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }

    isFalling = true; // 落下しているブロック
    fallingBlockNum = nextFallingBlockNum;
}


function moveRight() {
    // ブロックを右へ
    for (var row = 0; row & lt; 20; row++) {
        for (var col = 9; col & gt; = 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

function moveLeft() {
    // ブロックを左へ
    for (var row = 0; row & lt; 20; row++) {
        for (var col = 0; col & lt; 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col - 1].className = cells[row][col].className;
                cells[row][col - 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}